#!/bin/bash

gdisk /dev/sda
# Crear particiones

mkfs.fat -F32 /dev/sda1
mkfs.btrfs -L ROOT /dev/sda2

mount /dev/sda2 /mnt
cd /mnt
btrfs subvolume create @
cd
umount /mnt
mount -o noatime,space_cache=v2,compress=zstd,ssd,discard=async,subvol=@ /dev/sda2 /mnt
mkdir /mnt/boot
mount /dev/sda1 /mnt/boot

pacstrap /mnt base linux linux-firmware btrfs-progs intel-ucode git neovim

genfstab -U /mnt >> /mnt/etc/fstab

arch-chroot /mnt
