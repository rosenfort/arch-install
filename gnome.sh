#!/bin/bash

ln -sf /usr/share/zoneinfo/America/Argentina/Buenos_Aires /etc/localtime
sed -i '178s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "KEYMAP=la-latin1" >> /etc/vconsole.conf
echo "arch" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localdomain arch" >> /etc/hosts

sed -i 's/#ParallelDownloads = 5/ParallelDownloads = 4/g' /etc/pacman.conf
sed -i 's/#Color/Color/g' /etc/pacman.conf

echo root:password | chpasswd

useradd -m matt
echo matt:password | chpasswd
usermod -aG wheel matt

pacman -S --noconfirm efibootmgr base-devel xdg-user-dirs xdg-utils intel-media-driver gstreamer-vaapi

pacman -S --noconfirm gdm power-profiles-daemon gnome-control-center gnome-backgrounds gnome-console gnome-system-monitor nautilus gnome-text-editor gnome-calculator eog gnome-music gnome-clocks gnome-calendar clapper fragments firefox

sed -i 's/MODULES=()/MODULES=(btrfs)/' /etc/mkinitcpio.conf
mkinitcpio -p linux

bootctl --path=/boot install
sed -i 's/#timeout 3/timeout 5\ndefault arch/' /boot/loader/loader.conf
printf "title	Arch Linux\nlinux	/vmlinuz-linux\ninitrd	/intel-ucode.img\ninitrd	/initramfs-linux.img\noptions root=LABEL=ROOT rootflags=subvol=@ rw\n" >> /boot/loader/entries/arch.conf

systemctl enable gdm
systemctl enable NetworkManager
