#!/bin/bash

ln -sf /usr/share/zoneinfo/America/Argentina/Buenos_Aires /etc/localtime
sed -i '178s/.//' /etc/locale.gen
locale-gen
echo "LANG=en_US.UTF-8" >> /etc/locale.conf
echo "KEYMAP=la-latin1" >> /etc/vconsole.conf
echo "arch" >> /etc/hostname
echo "127.0.0.1 localhost" >> /etc/hosts
echo "::1       localhost" >> /etc/hosts
echo "127.0.1.1 arch.localdomain arch" >> /etc/hosts

sed -i 's/#ParallelDownloads = 5/ParallelDownloads = 4/g' /etc/pacman.conf
sed -i 's/#Color/Color/g' /etc/pacman.conf

echo root:password | chpasswd

useradd -m desu
echo desu:password | chpasswd
usermod -aG wheel desu

pacman -S --noconfirm efibootmgr base-devel xdg-user-dirs xdg-utils iwd intel-gpu-tools intel-media-driver gstreamer-vaapi
#pacman -S --noconfirm grub
pacman -S --noconfirm sway waybar foot bemenu bemenu-wayland mako grim wl-clipboard brightnessctl imv pipewire pipewire-pulse pipewire-jack mpd mpc ncmpcpp firefox ttf-iosevka-nerd ttf-fantasque-sans-mono adobe-source-han-sans-jp-fonts

sed -i 's/MODULES=()/MODULES=(btrfs)/' /etc/mkinitcpio.conf
mkinitcpio -p linux

#grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=GRUB #change the directory to /boot/efi is you mounted the EFI partition at /boot/efi

#grub-mkconfig -o /boot/grub/grub.cfg

bootctl --path=/boot install
sed -i 's/#timeout 3/timeout 5\ndefault arch/' /boot/loader/loader.conf
printf "title	Arch Linux\nlinux	/vmlinuz-linux\ninitrd	/intel-ucode.img\ninitrd	/initramfs-linux.img\noptions root=LABEL=ROOT rootflags=subvol=@ rw\n" >> /boot/loader/entries/arch.conf

printf "LIBVA_DRIVER_NAME=iHD\nMOZ_ENABLE_WAYLAND=1\nMOZ_DISABLE_RDD_SANDBOX=1\n" >> /etc/environment

sed -i '82s/..//' /etc/sudoers

timedatectl set-ntp true
hwclock --systohc
systemctl enable iwd
systemctl enable systemd-networkd
systemctl enable systemd-resolved
systemctl --user start mpd
systemctl --user enable mpd

printf "[Match]\nName=wlan0\n\n[Network]\nDHCP=yes" >> /etc/systemd/network/25-wireless.network
